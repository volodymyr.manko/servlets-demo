package com.spdu.serv;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;

@WebServlet(urlPatterns = "/user/*")
public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = Optional.ofNullable(req.getParameter("first_name")).orElse("");
        LocalDate.now();
        req.setAttribute("firstName", firstName);
        req.getRequestDispatcher("user.jsp").forward(req, resp);
    }
}
